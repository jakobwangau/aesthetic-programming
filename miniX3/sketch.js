//pumping variables
let x=50;
let y=70;

let movingSize=1;

//upload heart sound and clubby sound
let mySound;
function preload() {
  mySound = loadSound('assets/heartbeat.mp3');
  mySound2 = loadSound('assets/output1-2.mp3')
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  noStroke();
  frameRate(60);
}

function draw(){
  background(10,20);
  fill(255,20,-30+x)

  push();
  fill(255,random(255),random(255));
  textStyle(ITALIC);
  textSize(100);
  textAlign(CENTER);
  text('life is a beat',windowWidth/2,windowHeight/1.2);
  pop();

//inward moving
if(x && y > 100){
  movingSize = -2;
  mySound.play();
  mySound2.play();
  mySound2.setVolume(0.07);
}

//outward moving
if (x && y < 80){
  movingSize = 0.5;
}

//the variables change over time
x=x+movingSize;
y=y+movingSize;

//for loop creating multiple hearts on a line
for(var v = 0; v <= windowWidth; v += 150) {
ellipse(v-10,height/2+20,x+20,y+45);
ellipse(v, height / 2-20,x,y);
ellipse(v-30,height/1.9,x-10,y-15);
  }

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
}
