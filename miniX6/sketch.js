/* this program is a generative piece which is drawing the Berlin Wall over a
map of the city Berlin. The "wall" is generated as a rectangle moving slowly
and "randomly" across the screen. I have used a perlin noise influenced
greyscale color-scheme in order to make some more 2-dimensional effects.
At the bottom right corner is a counter starting at 1961 (year 1961 where
the Berlin Wall was built) which will reset the canvas after it reaches 1989
(year 1989 where the Berlin Wall was teared down).
*/

// variables fo perlin noise
let xOff1 = 0;
let yOff1 = 2000;

let xOff2 = 0;
let yOff2 = 2000;

let button;

let roboto;
let img;

let years = 1961;

//preloading image of a vector map of Berlin and a custom font
function preload(){
  roboto = loadFont('assets/Roboto-2/Roboto-Light.ttf');
  img = loadImage('assets/110513854-vector-map-of-the-city-of-berlin-germany.jpg')
}

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(255);

  // creating a reset button using CSS
  button = createButton('reset');
  button.position(10,18);
  // using hexcode to style the button background
  button.style('background','#DCAD27');

  // function that will reload the canvas
  button.mousePressed(reloadCanvas);

function reloadCanvas(){
    window.location.reload();
  }

  // creating the light brown canvas around the image
  stroke(200,170,0);
  strokeWeight(4);
  fill(255);
  rectMode(CENTER);
  rect(width/2,height/2,width-100,height-100);

  //preparing for the text in draw

  textFont(roboto);
  textSize(18);
  image(img, 50,50,width-100,height-100);
  fill(0);

}

function draw(){
  noStroke();
  textAlign(CENTER);
  rectMode(CENTER);
  fill(255);
  rect(width/2,25, 250,35);
  fill(0);
  text('the generative berlin wall', width/2,35);

  // get random number between 0 and 100
  push();
  var randomNumber = random(0, 100);
  // if randomNumber is greater than 99, add 1 to count
  if (randomNumber > 99) {
  	years = years + 1; // add 1 to count
  }  else if (years >= 1989) {
      window.location.reload(); //reset canvas if count is 1989
    }
  pop();

  // making the count variable in bottom right corner
  push();
  fill(255);
  rect(width-100, height-10, 160,75); // rectangle inserted to maintain the loop
  fill(0,0,0,255);
  strokeWeight(0.1);
  stroke(2);
  textSize(30);
  fill(0);
  text(years, width-60, height-15);
  pop();

  // variables for the perlin noise-based "random" movement
  let x = map(noise(xOff1),0,1,100,width-100);
  let y = map(noise(yOff1),0,1,100,height-100);

  // variable for the greyscale color
  let greyColors = map(noise(yOff2),0,1,0,255);

      fill(greyColors);
      rect(x,y,4,20);

  xOff1 += 0.002;
  yOff1 += 0.003;

  xOff2 += 0.01;
  yOff2 += 0.01;
}
