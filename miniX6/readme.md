**The Generative Berlin Wall**

[Link to the program!](https://jakobwangau.gitlab.io/aesthetic-programming/miniX6/)

![](photo1.jpg)
![](photo2.jpg)


For this miniX I have been revisiting the generative program again. This is due to the fact that I felt it was a good exercise to train syntaxes and different functions (play with conditional structures) and also strengthen the conceptual thinking behind generative programmes and "randomness" in general. My generative program from miniX5 was based upon the works of Ruth John and Tim Holman who has reconstructed the famous coverart of Joy Division's album Unknown Pleasures (1979) on their website "Generative Artistry" (Holman, John). For this week's miniX I wanted to explore code and randomness in a stronger conceptual process which somehow reversed some of my creative processes. I connected this idea of a generative program with our works with throbbers and a real-time representation of code in order to achieve my goal. 

I have created a program which is drawing a generatve figure of the Berlin Wall over a map of the city Berlin. The "wall" is generated as a rectangle moving slowly and "randomly" across the screen. I have used a perlin noise influenced greyscale color-scheme in order to make some more 2-dimensional effects. At the bottom right corner is a counter starting at 1961 (year 1961 where the Berlin Wall was built) which will reset the canvas after it reaches 1989 (year 1989 where the Berlin Wall was teared down). The work is based upon several syntatical structures which I have already used in my other miniX's (perlin noise in miniX5 and conditional structures from miniX3), as well as some of the assigned readings during the course.
I thought it was very interesting to create a stronger conceptual statement in the program, which could be an interpretable layer for the user, and my thoughts about autonomy in programming and human control of algorithms fuelled the idea of the Generative Berlin Wall. The idea is that the wall is randomly generated on the map of Berlin, restricting and entrapping citizens at random, which is of course devastating to individuals in the real world. This is a comment to the loss of autonomy, and states that the implications of algorithmic and technocratic society can have big consequences for us as a global civilisation. When applying something random, thus losing authority and autonomy, we also emphasize an anticipatory relation to programming, which makes it even harder to predict future events. 

The method I used was also quite different from the last generative program I made. In my last program (miniX5) my main approach was to explore "creative" capacities of the computer. The program was meant for the user to appreciate and explore the boundaries of creativity and the use of randomness as an artistic, conceptual approach. For this miniX my goal has been to change this to a more critical look on computational autonomy and power relationships between an individual person using the computer and the computer itself. I used the theoretical framework of critical making as a starting point for my experiments. This created a more solid cast conceptual project. I tried to conceptualize before I worked, but also discovered how the conceptualisation often shows itself through active participation in the process and the reflective practice with atom. As described below, aesthetic programming should atleast be as concerned with the conceptual, critical thinking as it should with the material conditions:

"Political aesthetics refers back to the critical theory of the Frankfurt School, particularly to the ideas of Theodor Adorno and Walter Benjamin, that enforce the concept that cultural production — which would now naturally include programming — must be seen in a social context. Understood in this way programming becomes a kind of “force-field” with which to understand material conditions and social contradictions, just as the interpretation of art once operated “as a kind of code language for processes taking place within society.” 

I tried to connect the conceptual framework with the social implications of the project to create this force field which made a more entangled and "useful" program in my opinion - at least it has a more directly critical dimension. My conceptual framework was based upon some of our readings of surveillance (for example _Surveillance & Capture_by Philip E. Agre). It was interesting to explore this relationship because the noise-generated randomness in my project somehow represents a public authority (literally the communist party in DDR from 1961-1989). On the other hand, knowing that this is a computer generated wall also positions the computer in an interesting place. At the same time, the counter in the bottom corner (the years) are also incrementing at a "random" pace (look at the source code). These concepts can further be elaborated through this quote: 

"(Even critical and self-aware capture systems) all participate in a trade-off that goes to the core of computing: a computer – at least as computers are currently understood – can only compute with what it captures; so the less a system captures, the less functionality it can provide to its users." (Agre, 749)

In the case of this program, the wall is generated at random, sometimes creating a pattern that makes sense, but most often tying knots on itself. This randomness creates an absurd image and it also illuminates the issue of human interest as opposed to computational efficiency. As Agre states, in order to provide meaningful output, the program has to get meaningful input (data about us). Since the input of this program is purely arbitrary, it decentralizes its output from the interests of the figurative people of Berlin by instead being absorbed by computer generated randomness. Agre is at the same time suggesting, that this interpretation of a computer may be solely temporal. In this case, my program is also a comment on how we should treat and and understand the computer as a posthuman object of study (Wolfe, 357). With this in mind, the artistry and independency of me as an artist in this work is also shown.


**References**

Agre, Philip E., _Surveillance & Capture - Two Modes of Privacy_, pp. 737-760, Information Society 10(2): 101–127. April–June, 1994.

Cox, Geoff & Soon, Winnie, _Aesthetic Programming - A Handbook Of Software Studies_, pp. 12-24, Open Humanities Press 2020

Wolfe, Cary. _Posthumansim._, Posthuman Glossary. pp. 356-359, Ed. Rosi Braidotti and Maria Hlavajova. Bloosmbury Publishing, 2018

_Vector Painting of the City Berlin_, URL: https://www.123rf.com/photo_110513854_stock-vector-vector-map-of-the-city-of-berlin-germany.html (visited 19.03.2021)

