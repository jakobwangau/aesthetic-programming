let øjne1Y = 0;
let øjne2X = -2;

function setup() {
  createCanvas(1400,1000);
  frameRate(10);
  noStroke();
}

function draw() {
  background(10, 0, 50);

  fill(255,255,255);

  //emoji 1

  //the face of the emoji
    ellipse(400,400,350,400,45);
    ellipse(300,280,100);

  //the blue parts of the emoji
  push();
    fill(30,200,220);
    ellipse(320,350,60);
    ellipse(480,380,60);
  pop();

  //the black parts of the emoji
  push();
    fill(0,0,0); //sort farve
    beginShape();
    vertex(500,480); //punkt 1 i munden
    vertex(300,450); //punkt 2 i munden
    vertex(400,550); //punkt 3 i munden
    endShape();
    ellipse(320,øjne1Y+360,40,30);
    ellipse(470,øjne1Y+380,30,40);

    stroke(1);
    curve(300,0,300,500,500,510,550,200);
  pop();

  //teeth
  push();
    fill(255,255,255);
    beginShape(); //højre tand
    vertex(430,450);
    vertex(430,500);
    vertex(400,510);
    vertex(400,460);
    endShape();

    beginShape(); //venstre tand
    vertex(380,470);
    vertex(380,600);
    vertex(340,590);
    vertex(340,480);
    endShape();
  pop();

  //emoji 2

    ellipse(1000,400,400,350,45);
    ellipse(900,480,150);

    //the pink color
    push();
      fill(255,150,200);
      ellipse(900,350,60);
      ellipse(1100,450,60);
    pop();

    //the black parts of the emoji
    push();
      fill(0,0,0); //sort farve
      beginShape();
      vertex(1100,300); //punkt 1 i munden
      vertex(850,450); //punkt 2 i munden
      vertex(1000,550); //punkt 3 i munden
      endShape();
      ellipse(øjne2X+900,360,40,50); // venstre pupil
      ellipse(øjne2X+1100,440,40,60); //højre pupil
    pop();

    //teeth
    push();
      fill(255,255,255); //hvid farve
      //højre tand
      beginShape();
      vertex(1030,500);
      vertex(1030,550);
      vertex(1000,510);
      vertex(1000,460);
      endShape();
      //venstre tand
      beginShape();
      vertex(980,470);
      vertex(950,520);
      vertex(900,530);
      vertex(900,500);
      endShape();
      //øverste tand
      beginShape();
      vertex(1090,300);
      vertex(1020,300);
      vertex(1000,400);
      vertex(1000,400);
      endShape();
    pop();

    if (øjne1Y<4)
    øjne1Y=øjne1Y+1;
    else
    øjne1Y=øjne1Y-4;

    if (øjne2X<4)
    øjne2X=øjne2X+1;
    else
    øjne2X=øjne2X-4;
}
