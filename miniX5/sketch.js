//variable which changes the y-positions of the vertexes
let f=0;

function setup(){
    //creating dynamic canvas
    createCanvas(windowWidth,windowHeight);
}

function draw(){
    //defining the vertex as a variable
    let v=vertex;
	  background(255,150);
    fill(255);
	  stroke(0);
    //changing strokeweight creating a dynamic to the vertexes
	  strokeWeight(random(1.5,2));

    //looping through incrementations of f so that the variable is constantly changing
    f++;

    //for loop going through y-coordinates
    for(y=0;y<height;y+=40){
		beginShape();
		  //nested for-loop going through x-coordinates and creating vertexes which exponentially distorts
          for(x=0;x<width;x++){
		  v(x,y-200/(1.5+pow(x-width/4,3)/8e10)*noise(x/100+f/10+y));
      }
      //creating shapes with the incrementing f-variable as y-coordinate
      v(x,f);
		endShape();
  }
    //if statement which returns the program to its starting point by changing the value of f to 0
    if(f>width){
    f-=width;
  }
}


function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}
