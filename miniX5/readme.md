[Here's a link to my RunMe!](https://jakobwangau.gitlab.io/aesthetic-programming/miniX5/)

![](screenshot1.jpg)
![](screenshot2.jpg)

**ReadMe**

For miniX5 I have made a program which generates a pattern of distorted lines running across the screen slowly trying to fill the canvas. The lines run across the screen (from left to right) over time creating a dynamic motion which is amplified by their distorted movements across the canvas.

My program took its inspiration from the work of Ruth John and Tim Holman (Generative Artistry) which is a paraphrase of the classic cover of Joy Divisions "Unknown Pleasures" from 1979. I looked up the source code on the internet and used this as a template for my work. I then manipulated the source code and added new elements to make it more unique in its appearance. I also used the noise effect (perlin noise) from the p5.js library as well as "pow" (which facilitates exponential expressions) to make it possible to create the distorted expression. 

The program contains a for loop with a nested loop inside, containing several variables to create a constant generating motion on the screen. The rules in my generative program are that my variables x and y (placed in two loops) creates several vertexes, and that these vertexes move in unanticipated (random) ways across the x-axis of the canvas.The two loops makes it possible to both have a dynamic x-position and y-position of the lines. These coordinates are then put into a vertex (which I call v) with different statements to make a "random" generative appearance. This is achieved through use of the function "pow" and perlin noise as well as the variable f, which is constantly incrementing outside the loops. This creates a subtle motion, slowly filling the whole canvas with lines (from top to bottom). I then implemented a conditional statement (if) in order to reset the canvas, so the program is "looping" (though it is randomized). I actually wanted to make a more smooth transition to make the generative attributes more central to the aesthetic experience, but did not know how to (showcasing my lack of skills in p5.js). The rules produce constant motions and distortion in the canvas, creating the illusion of an infinite "race" between the canvas and the black lines. The generative code can also be seen as a figurative drawing of mountainous landscapes and I really like the unpredictability of the generative code aesthetic.

What has been quite interesting in this week has been the displacement of the artist in works of generative code. It fragmentizes and distances the artist from the actual "performance" of the art, but at the same time it also grants new artistic and aesthetic possibilities as well as experiences. The supremacy of creation lies within the computer, and the anticipation of the artist is often a necessity. Cox and Soon comments on the relationship, adressing the possibilities of computational "creativity":

"Recursive fractal geometry and flocking behaviors are examples that demonstrate “entropic” qualities (lack of order or predictability) based on the self-organization of computation and autonomous agents that evolve over time. This kind of approach is important, not only because it offers a different way of drawing and authoring works by machines based on mathematical logic, but also to provide a sense of machine creativity that — as in the previous chapter — negates intentionality and questions the centrality of human (more often than not, masculinist) agency." (Cox, Soon, pp. 125)

This different way of drawing (which is somehow anticipatory because it is computational) is beyond human imagination, which makes it seem random, thus creating an understanding of the machine as generating something out of a creative, undefined (non-coded) space. This is of course an illusion, because it is anticipated in the source code. In my program the computer generates several lines from quite few lines of code and they move in unexpected directions for the human eye, though it is not random in the way that the "pow"-syntax is exponential, thus predictable for a computer (even though it seems unpredictable to us due to our lesser processing powers). The auto-generation is therefore facilitated by me (as the artist), but can only be executed by a computer.


**References**

Cox, Geoff & Soon, Winnie, _Aesthetic Programming - A Handbook Of Software Studies_, pp. 121-142, Open Humanities Press 2020

John, Ruth & Tim Holman - URL: https://generativeartistry.com/tutorials/joy-division/


Source code for JavaScript used as template in this exercise - URL: https://editor.p5js.org/jcponce/sketches/aHzYss9tz

// Original code by Craig S. Kaplan
// https://www.openprocessing.org/user/167697
// https://www.openprocessing.org/sketch/683686

f=0
setup=draw=_=>{
  createCanvas(a=600,b=400)
	v=vertex
	f++
	background(0)
	fill(0)
	stroke(b) 
	for(y=100;y<b;y+=5) {
		beginShape()
		for(x=0;x<a;++x)
		  v(x,y-80/(1+pow(x-a/2,4)/8e6)*noise(x/30+f/50+y))
		  v(x,1e4)
		endShape()
	}
}
