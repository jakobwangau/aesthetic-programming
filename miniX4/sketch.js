/* Here's my RunMe. I have made program where I have used a list of current genders,
and made it possible to interact with them with your voice. When you speak, the genders will blur together, stating the fluency of identity
in our postmodern society. It only leaves the camera of your face.
*/

//array with list of genders
let n = ['male','female','nonbinary','genderfluid','agender','androgyne','intersex','trans','queer','pansexual','butch','demigender','neuter',];


//functions for webcam, audio and font
let capture;
let mic;
let roboto;
let vol;


//preload font
function preload(){
  roboto = loadFont('assets/Roboto-Black.ttf');
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(10);

  //audio capture
   mic = new p5.AudioIn();
   mic.start();

  //web cam capture
 capture = createCapture(VIDEO);
 capture.size(640, 480);
 capture.hide();
}

function draw() {
  background(0,200);

  userStartAudio();

  let micLevel = mic.getLevel();

  //map audio to size
  let textSize1 = map(micLevel,0,1,10,200);

  //for loop going through the arrays
  for (x = 0; x <= width; x+=50){
    for (y = 0; y <= height; y+=10){
      fill('#07F703');
      textSize(textSize1);
      textFont(roboto);
      text(random(n),x,y);
    }
  }

  //draw the captured video on a screen
  image(capture,width/2-70,height/2-100, 200,140);
}

function windowResized(){
    resizeCanvas(windowWidth, windowHeight);
}
