//A(I) FAIRYTALE source code (by Jacob Boutrup & Jakob Stougaard Wang)

let title = ['duckling', 'teapot', 'grim', 'chicken', 'old',
'grandmother', 'moon', 'Ole', 'winds', 'traveling', 'girl', 'matches', 'tinder',
'road', 'evening', 'clumsy', 'golden', 'Lukoje',
'lucky', 'little', 'box', 'play', 'tree', 'nightingale', 'queen', 'mermaid', 'poor', 'Hans',
'shadow', 'grocer', 'son', 'little', 'church', 'brothers', 'happiest', 'beautiful',
'god']; //array going through words for generating the title of the fairytale

let beginning;
let filling;
let ending;

let titleText;
let writeTitletext;
let speakTitletext;

let beginText;
let speakBegintext;

let endText;
let speakEndtext;

let allText = "";

let myVoice = new p5.Speech(); // new P5.Speech object

function preload(){
  beginning = loadJSON("beginnings.json")
  filling = loadJSON("fill.json");
  ending = loadJSON("endings.json");
}

function setup() {
  createCanvas(windowWidth, windowHeight);

  background(0);

  let begin = beginning.beginnings;
  let end = ending.endings;
  let fillings = filling.fills;

  fill(255);
  textFont('Times New Roman');

  // the title of the fairytale chosen from title array
  textSize(30);
  titleText = "The" + " " + random(title) + " " + random(title) + " " + random(title);
  writeTitletext = text(titleText, width/2-200, 120);
  speakTitletext = titleText;

  textSize(12);

  // looping through JSON of filltext
  for (let i = 0; i < 400; i+=20){
    let fillText = random(fillings);
    allText = allText + " " + fillText;
  }

  // the first sentence of the fairytale chosen from JSON of beginnings
  beginText = random(begin);
  speakBegintext = beginText;

  // the last sentence of the fairytale chosen from JSON of endings
  endText = random(end);
  speakEndtext = endText;

  // generating all text
  text(beginText + allText + " " + endText, width/2-200, 150, 400, 1000);

  text('A(I) FAIRYTALE', 20, height-20);
  textSize(9);
  text('by Jacob Boutrup & Jakob Stougaard Wang', 20, height-40)

  // computer-generated voice reading the fairytale aloud using the p5.speech library
  myVoice.speak(titleText + "," + speakBegintext + " " + allText + " " + speakEndtext);

  // creating a button for reloading canvas using CSS
  button = createButton('NEXT FAIRYTALE');
  button.position(20,20);
  button.style("background","none");
  button.style("display", "inline-block");
  button.style("font-size", "0.7em");
  button.style("color", "#FFFFFF");
  button.style("border", "1px solid #F4F4F4");
  button.style("padding", "10px 25px 10px 25px");
  button.style("font-family", "Arial");
  button.style("font-weight", "Bold");
  
  // function that will reload the canvas
  button.mousePressed(reloadCanvas);
}

function reloadCanvas(){
    myVoice.stop();
    window.location.reload();
}
