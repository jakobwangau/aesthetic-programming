**miniX7**

[SNOWDODGER](https://jakobwangau.gitlab.io/aesthetic-programming/miniX7/) - OPEN IN CHROME FOR SOUND EFFECTS

![](snowdodger1.jpg)

**Reflections on classes and objects**

For this miniX we have been told to create a class-based, object-oriented sketch which is interactable (a kind of simple game). I have been thinking about the influence on object-oriented programming and its relation to the physical world. Classes and objects are an abstraction of code, and it also reflects how we can choose to look at society (such as Karl Marx did). This is elaborated in _Aesthetic Programming, Handbook of Software Studies_:

"With the abstraction of relations, there is an interplay between abstract and concrete reality. The classifications abstract and concrete were taken up by Marx in his critique of capitalist working conditions to distinguish between abstract labor and living labor. Whereas abstract labor is labor-power exerted producing commodities that uphold capitalism, living labor is the capacity to work." (Cox, Soon, 161)

This interplay between abstract and concrete reality is fundamentally what object oriented programming (OOP) is all about. It is an interplay, because on the one hand the abstraction of thinking of a square made in p5.js as an object with properties and abilities is making it possible to somehow connect this particular square to squares we know from our real world. On the other hand, this approach (this abstraction) also makes the square become an almost parodical representation of a square with only a limited set of properties and commodities that we (the designers) have chosen. In this way it is at the same time a more full-bodied and a more stereotypical representation of the square. 

**Snowdodger**

![](snowdodger2.jpg)

Based on these thoughts I wanted to make a game, which aims to distinguish between abstract objects and simple representations and this has culminated in my own game, which I call "Snowdodger". It is a simple game (many games such as for example Cubefield uses the same game-mechanical template) in which I have tried to create objects which appears to be "more" than just simply code. The game consists of several different syntaxes and must be my most complex miniX so far. By using the cursor on the mouse, the player steers a very basic stick figure. The objective of the game is to avoid snow falling from the sky (this idea is a remake of the tofu game introduced to us in class), and the game gets harder the longer the stick figure survives. The stick figure (or avatar) has 10 hitpoints and in order to win the game the player has to dodge 2000 snowflakes spread out on 10 levels. Everytime a snowflake "hits" the stick figure it loses 1 hitpoint.

I find it very interesting how object oriented programming can create stereotypical, yet persuasive representations of real objects. When I play the game, I have created it aesthetically "feels" like the materiality of the snow is more dense than if I simply looked at a generative program with falling white ellipses (which it basically is). To strengthen the aesthetic notion of the white ellipses having a materiality, I played with sound effects so that everytime the stick figure is hit by the snow, a (rather comical) sound is played. This both influences the "density" of the snow as well as the joy of playing, because of its absurd dimension, but also because of its connection the physical world. By implementing sounds and movement to the sprite (the object), it becomes a representation of the player playing the game - it somehow becomes an extension of the player. Another dimension is the idea of hitpoints which also increases the complexity and representation of the object and its connection to humans. 


**References**

_Cubefield_, URL: https://www.cubefield.org.uk

Cox, Geoff & Soon, Winnie, Aesthetic Programming - A Handbook Of Software Studies, pp. 145-164, Open Humanities Press 2020

