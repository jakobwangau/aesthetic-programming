class Snowflakes {
  constructor() {
    this.speed = floor(random(2,10));
    this.size = floor(random(15,35));
    this.pos = new createVector(random(0,width),0);
  }
  fall(){
    this.pos.y += this.speed;
  }
  show(){
    noStroke();
    fill(255);
    ellipse(this.pos.x, this.pos.y, this.size, this.size);
  }
}
