let minSnow = 30;
let snow = [];
let img;
let themeSound;
let punchSound;
let gameoverSound;
let winSound;
let score = 0;
let sprite;
let health = 10;
let spriteSize;
let button;
let level = 1;

function preload(){ //preloading sounds and background
  img = loadImage('assets/snowy_landscape.jpg');
  punchSound = loadSound('assets/Punch.mp3');
  gameoverSound = loadSound('assets/Aww.mp3');
  themeSound = loadSound('assets/snowdodger.mp3');
  winSound = loadSound('assets/win.mp3');
}

function setup(){
  createCanvas(windowWidth,700);
  spriteSize = {
    w:100,
    h:1000
  };
  themeSound.play(); //starting the themesong once
}

function draw(){
  background(255);
  image(img, 0, 0, windowWidth, 600);
  checkSnowNum();
  showSnow();
  checkSnowFall();
  checkSnowHit();
  showSprite();
  lose();
  levelUp();
  fill(255);
  stroke(0);
  strokeWeight(1);
  textSize(30);
  textAlign(CENTER);
  text('score:', width-150,50);
  text(score,width-50,50);
  text('lives:',width-150,100);
  text(health,width-50,100);
  text('level:', width-150,150);
  text(level, width-50,150);
  fill(0);
  text('welcome to snowdodger', 200, 640);
  textSize(15);
  strokeWeight(0.5);
  text('dodge the snowflakes by using the cursor. you gain points for each snowflake you dodge.', 335, 670)
  text ('you have 10 lives in total. good luck.', 160, 690);
}

function showSprite(){ //uploading the sprite as an object
  sprite = new Sprite();
}

function checkSnowNum(){ //making sure the programme keeps producing more and more snowflakes
  if (snow.length < minSnow) {
    snow.push(new Snowflakes(floor(random(2,5))));
  }
}

function showSnow(){ //uploading snowflakes as objects
  for(let i = 0; i < snow.length; i++){
    snow[i].fall();
    snow[i].show();
  }
}

function checkSnowFall(){ //deleting snowflakes when out of canvas
  for (let i = 0; i < snow.length; i++){
    if (snow[i].pos.y > height){
      snow.splice(i,1);
      score++;
    }
  }
}

function checkSnowHit() { //deleting snowflakes if they hit the sprite
  //calculate the distance between each snowflake
  for (let i = 0; i < snow.length; i++) {
    let d = int(
      dist(mouseX, spriteSize.h/2,
        snow[i].pos.x, snow[i].pos.y)
      );
    if (d < spriteSize.w/2.5) { //close enough as if getting hit by a snow snowflake
      health--;
      snow.splice(i,1);
      punchSound.play();
    } else if (snow[i].pos.x < 3) { // sprite dodges the snow flakes
      score++;
      snow.splice(i,1);
    }
  }
}

function levelUp(){ //increasing difficulty by creating more snowflakes to dodge
  if (score == 200){
    minSnow ++;
    level=2;
  } else if (score == 400){
      minSnow += 2;
      level=3;
  } else if (score == 600){
      minSnow ++;
      level=4;
  } else if (score == 800){
      minSnow += 2;
      level=5;
  } else if (score == 1000){
      minSnow += 2;
      level=6;
  } else if (score == 1200){
      minSnow += 2;
      level=7;
  } else if (score == 1400){
      minSnow ++;
      level=8;
  } else if (score == 1600){
      minSnow +=2;
      level=9;
  } else if (score == 1800){
      minSnow +=3;
      level=10;
  } else if (score == 2000){ //winning condition
    fill(0,255,200);
    textSize(150);
    noStroke();
    text('YOU WON!', width/2, height/2-100);
    themeSound.stop();
    winSound.play();
    noLoop();
    // creating a 'play again' button using CSS
    button = createButton('play again');
    button.position(width-180,height-80);
    // using hexcode to style the button background
    button.style('background','#33F09D');
    button.size(150,50);
    // function that will reload the canvas
    button.mousePressed(reloadCanvas);

    function reloadCanvas(){
        window.location.reload();
    }
  }
}

function lose(){ //losing condition
if (health <= 0) {
  fill(255,0,0);
  noStroke();
  textSize(150);
  text('GAME OVER', width/2, height/2-100);
  textSize(100);
  text('YOUR TOTAL SCORE WAS', width/2, height/2+100);
  text(score, width/2, height/2+215);
  gameoverSound.play();
  themeSound.stop();
  noLoop();
  // creating a 'play again' button using CSS
  button = createButton('play again');
  button.position(width-180,height-80);
  // using hexcode to style the button background
  button.style('background','#33F09D');
  button.size(150,50);
  // function that will reload the canvas
  button.mousePressed(reloadCanvas);

function reloadCanvas(){
    window.location.reload();
    }
  }
}
