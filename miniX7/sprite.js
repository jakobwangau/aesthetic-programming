class Sprite {
  constructor(){
    fill(0,255,200);
    ellipse(mouseX,height-240,50,50);
    stroke(0,255,200);
    strokeWeight(20);
    line(mouseX,height-250,mouseX,height-170);
    line(mouseX,height-200,mouseX-40,height-190);
    line(mouseX,height-200,mouseX+40,height-190);
    line(mouseX,height-170,mouseX+30,height-110);
    line(mouseX,height-170,mouseX-30,height-110);
  }
}
