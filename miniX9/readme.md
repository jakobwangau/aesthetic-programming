[THE FLAT EARTH SATELLITE](https://jakobwangau.gitlab.io/aesthetic-programming/miniX9/) (there is a problem with the API on GitLabs public servers, so we recommend you to download the source code and open the program on a local server in Atom)

**The Flat Earth Satellite**

![](flatearthpic1.jpg)

For this miniX, we have worked with data extraction by using API’s (Application Programming Interface). We have used and investigated the API which can tell us the position of the international space station (ISS). This data extraction has laid the foundation for our work. In the programme we wanted to emphasize a critical reflection of the issue related to “the universal truth”, and this is manifested in “The Flat Earth Satellite”. In the modern age, we face not only a single truth, but many different (often contradicting) views upon reality. The latest example is probably the whole Qanon-movement, but also the conspiratory movement called Flat Earth, which has been growing in the past couple of decades. This movement does (as the name implies) think that planet earth is flat, as opposed to the broad agreement between scientists (as well as most common people) that the earth is a sphere. The modern digital age constitutes these contradicting experiences of reality, partly because of the democratization of science and knowledge-sharing through for example Web 2.0:
An obvious explanation for the growing number of conspiracy theories and their rise in recent decades can be found in the rapid development of our approach to knowledge. Knowledge production has been democratized, as the official channels have been supplemented with new and more open and uncontrolled sources. (..) The now common term 'web 2.0' refers partly to the more user-friendly design of social network technologies, which enables even people with limited technological knowledge to use them, and partly to create more dynamic content, where users are involved in the process of creating and sharing the content. (Peters, Rikke Louise Alberg, Mikkel Thorup, and Maria Brockhoff, p. 164) 
As the authors of the book clearly states, technology plays a large role in the creation of conspiracy theories, namely because technological developments have created the opportunity for individuals to go online and write their own truths. We have in this matter created a programme that tries to emphasize the flat earth conspiracy through use of the data extracted from the API of ISS. We have then reinterpreted the position of the space station with the mindset of a flat earther (as opposed to the “real” position of ISS, which can be seen here: https://spotthestation.nasa.gov/tracking_map.cfm). In this matter, the programme investigates the issue of the strive for the universal truth and the inherent individual realities of modern society. If we wish to, we can critique, question and deny every aspect of our existence, constituting our own world-making, but what is the cost of this trade? The democratization and political polarisation also play a central role in this matter. Most of the questioning of what seems as “fundamental truths” has been visible in the USA, where different gatherings and organizations denying what to many seems as fundamental truths have been spreading to the rest of the world - in Denmark, this could be the Men In Black (denying and opposing against restrictions by the government due to the Covid19-pandemic) or the whole anti-vax movement. This could be due to the massive political polarisation and the many opposing groups in the country. This combined with the glorification of freedom of speech and liberty could have been constituting the general disagreement of basic “facts”. Donald Trump and his fake news campaigns is a good example of this problem. The question is who is right - and how do we live side by side if we have fundamentally different perceptions of the world we share? 

![](flatearthpic2.jpg)

**Data Extraction**

Another issue we have discussed in the work with our miniX9 is the validity of data-extraction and the corporate interests in the sharing of information. How do we “check” if the data we withdraw from the web pages are “true”? This issue does in many ways relate to our conceptual thoughts about the Flat Earth movement and the individual world making, but at the same time it also shows the capitalist monopolized power relation of data - and the world making it constitutes. Dataism is in a way the global, modern religion, because every aspect of reality can be seen as data, used as data, and turned into capital - which is the fundament the globalized, capitalist society is built upon. In this matter, the owners of the API’s and databanks are the ones shaping the reality and the “truth”, because so many studies, companies and people are dependent on the validity of the data used in their projects. Cox & Soon also address this issue in their book Aesthetic Programming - A Handbook of Software Studies:

“As noted in the introduction, simple operations such as search or feeds order data and reify information in ways that are clearly determined by corporate interests. The politics of this resonates with what Antoinette’s Rouvroy’s phrase “algorithmic governmentality” (the second part of which combines the terms government and rationality) to indicate how our thinking is shaped by various techniques.” (Cox & Soon, 206)

We thought the idea of “algorithmic governmentality” is a very interesting phenomena, in the way that the shaping of our world and thoughts about it, is shaped by these techniques and politics. All in all, the tech giants (such as Facebook, Amazon and Google) are companies with the basic capitalist goals - to make profit. It is important to have this in mind, when working with API’s and data extraction, because it is such an inherent part of its existence. In this week's work with miniX9 we have encountered many problems with APIs. Some of these problems were that with many of the APIs you had to register to be allowed to use them and in some cases it could take up to a week before you got access to your API key. Other frustrations we experienced were that in many APIs there is a limit to how many times you can access the Data and that this was a problem if we wanted to create a dynamic program that constantly retrieved new data. 


**If we had more time..**

If we had had more time we would have liked to work with some larger datasets for example Wikipedia's API. It was actually our original plan to use Wikipedia's API. However, we quickly realized that this was very laborious and that it required more than a week to create something interesting. It was also very hard for us to identify how to retrieve the data from the API (finding the API-key and signing up the right way). It would have been interesting to extract and use a JSON-file with a more complex structure in order to make a more “dense” programme. 

If we had worked with Wikipedia's API we would have worked with the question of what truth is in another way. Precisely because Wikipedia is an open page where "everyone" can contribute information and knowledge. How do you define a truth when everyone can contribute their personal take on the truth?


**Resources and literature**

API: http://api.open-notify.org/iss-now.json

MAP: https://spotthestation.nasa.gov/tracking_map.cfm

Peters, Rikke Louise Alberg, Mikkel Thorup, and Maria Brockhoff. Den skjulte sandhed: Konspirationsteorier, magt og konflikt. Klim, 2018

Cox, Geoff & Soon, Winnie, Aesthetic Programming - A Handbook Of Software Studies, pp. 168-186, Open Humanities Press 2020



