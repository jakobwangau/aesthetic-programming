//miniX9 by Anders Opstrup Christensen & Jakob Stougaard Wang - THE FLAT EARTH SATELITE

let spaceStation;

let url = 'https://api.open-notify.org/iss-now.json?fbclid=IwAR0hVqctup_ZcHCyNl27zh61f1RU3kmto9YZ6bkwZxlbji6pUEIrrtu37-8';

let posX;
let posY;

let img;

let lat;
let long;

function preload(){
	img = loadImage('assets/world.svg.png'); // preloading background image
}

function setup() {
	createCanvas(windowWidth,windowHeight);
	setInterval(spaceX, 100); //updating every 100 miliseconds

	img.resize(windowWidth,0);
}

function spaceX(){
	loadJSON(url, gotData);
}

// return function
function gotData(data){
	spaceStation = data;
	lat = data.iss_position.latitude;
	long = data.iss_position.longitude;

	posX = map(lat,-180,180,0,width);
	posY = map(long,-180,180,0,height);
}

function draw() {
	background(0,200,255);

	imageMode(CENTER);
	image(img, width/2,height/2);

	if (spaceStation){
		fill(255);
		stroke(255);
		line(width/2,0,width/2,height);
		line(0,height/2,width,height/2);
		noStroke();
		ellipse(posX, posY, 15);
		textAlign(CENTER);
		textSize(9);
		text(lat + "°",posX,posY+20);
		text(long + "°",posX,posY+30);
		text('180°', width-15,height/2+15);
		text('-180°', 15,height/2+15);
		text('-180°', width/2+15,15);
		text('180°', width/2+15,height-15);
		textSize(11);
		text('THE FLAT EARTH SATELLITE', 110, height-40);
		text('by Anders Opstrup Christensen & Jakob Stougaard Wang', 180, height-20);
	}
}
