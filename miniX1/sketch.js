let waveY = 300;
let waveE = 300;
let sunY = -60;
let moonY = 630;
let skyY = 220;
let sunfillY = 255;


function setup() {
  // put setup code here
  createCanvas(590,450);
  print("hello world");
  noStroke();
}


function draw() {
  // put drawing code here
  background(0,skyY+10,skyY+25);

  fill(255,255,255);
  ellipse(moonY,150,70);

  fill(235,sunfillY,0);
  ellipse(sunY,100,70);

  fill(20,100,200,random(100,50));
  rect(-10,waveE+20,30,300,160);
  rect(10,waveY,30,300,160);
  rect(40,waveE,30,300,160);
  rect(70,waveY-10,30,300,160);
  rect(100,waveE,30,300,160);
  rect(130,waveY,30,280,160);
  rect(160,waveE,30,280,160);
  rect(190,waveE+10,30,270,160);
  rect(220,waveE,30,270,160);
  rect(250,waveY,30,260,160);
  rect(280,waveE,30,260,160);
  rect(310,waveY,30,250,160);
  rect(340,waveE,30,230,160);
  rect(370,waveY,30,240,160);
  rect(400,waveY+10,30,270,160);
  rect(430,waveE+10,30,230,160);
  rect(460,waveE,30,300,160);
  rect(490,waveY,30,220,160);
  rect(520,waveE,30,250,160);
  rect(550,waveY,30,210,160);
  rect(570,waveE-20,25,230,160);

  fill(25,150,250);
  rect(-10,waveE+20,20,300,160);
  rect(10,waveY,20,300,160);
  rect(40,waveE,20,300,160);
  rect(70,waveY-10,20,300,160);
  rect(100,waveE,20,300,160);
  rect(130,waveY,20,280,160);
  rect(160,waveE,20,280,160);
  rect(190,waveE+10,20,270,160);
  rect(220,waveE,20,270,160);
  rect(250,waveY,20,260,160);
  rect(280,waveE,20,260,160);
  rect(310,waveY,20,250,160);
  rect(340,waveE,20,230,160);
  rect(370,waveY,20,240,160);
  rect(400,waveY+10,20,270,160);
  rect(430,waveE+10,20,230,160);
  rect(460,waveE,20,300,160);
  rect(490,waveY,20,220,160);
  rect(520,waveE,20,250,160);
  rect(550,waveY,20,210,160);
  rect(580,waveE-20,20,230,160);


  frameRate(45);


if(waveY<330)
  waveY = waveY+2;
else
  waveY = waveY-50;

if(waveE>275)
  waveE = waveE-20;
else
  waveE = waveE+1;

if(sunY<900)
  sunY = sunY+1;
else
  sunY = sunY-900;

if(sunY>300)
  skyY = skyY-0.5;
else
  skyY = 220;

if(sunY>630)
  skyY = skyY+1.1;

if(sunY>630)
  moonY = moonY-2.6
else
  moonY=630;

if(sunY>300)
  sunfillY = sunfillY-0.5;
else
  sunfillY = 255;

}
